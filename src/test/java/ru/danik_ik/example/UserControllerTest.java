package ru.danik_ik.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserControllerTest {

    private final String NAME = "Trololo";
    @Mock
    private UserDao userDao;
    private UserController it;

    @BeforeEach
    void setUp() {
        it = new UserController();
        it.userDao = userDao;
    }

    @Test
    void addUserAndGet() {
        when(userDao.saveUser(any())).thenAnswer(invocationContext -> invocationContext.getArguments()[0]);
            // возвращаем как есть первый аргумент метода

        var user = it.addUserAndGet(NAME);
        assertEquals(NAME, user.getName());
    }

    @Test
    void getAllUsers() {
        when(userDao.getAllUsers()).thenReturn(Collections.singletonList(sampleUserEntity()));

        var users = it.getAllUsers();
        verify(userDao, times(1)).getAllUsers();
        assertEquals(1, users.size());
        assertEquals(NAME, users.get(0).getName());
    }

    @Test
    void getUserById() {
        final int USER_ID = 7;
        when(userDao.getUsersById(any(int.class))).thenAnswer(ctx -> {
                    return sampleUserEntity(((int) ctx.getArguments()[0]));
                });
            // any() без указания класса для примитива вызовет ошибку!

        var user = it.getUsersById(USER_ID);
        verify(userDao, times(1)).getUsersById(USER_ID);

        assertEquals(USER_ID, user.getId());
        assertEquals(NAME, user.getName());
    }

    private UserEntity sampleUserEntity() {
        return sampleUserEntity(1);
    }

    private UserEntity sampleUserEntity(int id) {
        var it = new UserEntity();
        it.setId(id);
        it.setName(NAME);
        return it;
    }
}