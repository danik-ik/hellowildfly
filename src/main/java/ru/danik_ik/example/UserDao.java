package ru.danik_ik.example;

import java.util.List;

public interface UserDao {
    UserEntity saveUser(UserEntity user);

    List<UserEntity> getAllUsers();

    UserEntity getUsersById(int id);
}
