package ru.danik_ik.example;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


@ApplicationPath(RestApplication.API_URL)
public class RestApplication extends Application {
    static final String API_URL = "/demo";
    private static final String ENCODING_PROPERTY = "encoding";

    private Set<Class<?>> classes = new HashSet<>();

    public RestApplication(){
        registerResourceClasses();
        registerExceptionMapperClasses();
        registerFilterClasses();
    }

    private void registerResourceClasses() {
        register(UserController.class);
    }

    private void registerExceptionMapperClasses() {
        // TODO: обработчик исключений
    }

    private void registerFilterClasses() {
        // TODO: фильтры (eсли надо)
    }

    private void register(Class clazz){
        classes.add(clazz);
    }

    @Override
    public Set<Class<?>> getClasses() {
        return classes;
    }

    @Override
    public Map<String, Object> getProperties() {
        return new HashMap<String, Object>(){{
            put(ENCODING_PROPERTY, StandardCharsets.UTF_8.toString());
        }};
    }
}