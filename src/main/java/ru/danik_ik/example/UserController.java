package ru.danik_ik.example;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/")
public class UserController {

    @Inject
    UserDao userDao;

    @GET // Get -- чтобы можно было пощупать не имея ничего, кроме браузера. Конечно, здесь должен быть @PUT.
    @Path("/addUser")
    @Produces(MediaType.APPLICATION_JSON)
    public UserEntity addUserAndGet(@QueryParam("name") String userName) {
        UserEntity user = new UserEntity();
        user.setName(userName);
        return userDao.saveUser(user);
    }

    @GET
    @Path("/user")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UserEntity> getAllUsers() {
        return userDao.getAllUsers();
    }

    @GET
    @Path("/user/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UserEntity getUsersById(@PathParam("id") int id) {
        return userDao.getUsersById(id);
    }

}
