package ru.danik_ik.example;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class UserDaoImpl implements UserDao {

    @PersistenceContext(unitName = "myUnit")
    EntityManager entityManager;

    @Override
    public UserEntity saveUser(UserEntity user){
        return entityManager.merge(user);
    }

    @Override
    public List<UserEntity> getAllUsers() {
        return entityManager.createQuery("Select u from UserEntity u", UserEntity.class).getResultList();
    }

    @Override
    public UserEntity getUsersById(int id) {
        var query = entityManager.createQuery("Select u from UserEntity u where u.id = :id", UserEntity.class);
        query.setParameter("id", id);
        return query.getSingleResult();
    }

}