#!/bin/bash
mvn clean archetype:create-from-project -DarchetypeGroupId=ru.danik_ik -Darchetype.encoding=UTF-8

# добавляются в проект глобальные игноры
echo "gitignore=.gitignore">>target/generated-sources/archetype/src/test/resources/projects/basic/archetype.properties
cp .gitignore target/generated-sources/archetype/src/main/resources/archetype-resources/__gitignore__
sed -iE "s/\.gitignore/__gitignore__/" target/generated-sources/archetype/src/main/resources/META-INF/maven/archetype-metadata.xml
sed -iE "s/\bhelloWildFly.iml\b/__artifactId__.iml/" target/generated-sources/archetype/src/main/resources/META-INF/maven/archetype-metadata.xml
sed -iE "s/<fileSets>/<requiredProperties>\n    <requiredProperty key=\"gitignore\">\n      <defaultValue>\.gitignore<\/defaultValue>\n    <\/requiredProperty>\n  <\/requiredProperties>\n  <fileSets>/" target/generated-sources/archetype/src/main/resources/META-INF/maven/archetype-metadata.xml
